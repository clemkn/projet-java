package graph.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.Entreprise;

public class AddEntrepriseFrame extends JFrame {
	
	private JPanel panel;
	private JLabel lbSiret, lbId, lbNom, lbAdresse, lbTel;
	private JTextField jtfSiret, jtfNom, jtfAdresse, jtfTel;
	private JButton btnAjouter, btnQuitter;
	
	public AddEntrepriseFrame(EntreprisesJTable table){
		
		setSize(300,300);
		
		setTitle("Ajouter");
		panel = new JPanel();
		panel.setLayout( new GridLayout(4,2,10,10) );
		
		lbSiret = new JLabel("SIRET: ");
		panel.add(lbSiret);
		jtfSiret = new JTextField();
		panel.add(jtfSiret);
		
		lbNom = new JLabel("Nom: ");
		panel.add(lbNom);
		jtfNom = new JTextField();
		panel.add(jtfNom);
		
		lbAdresse = new JLabel("Adresse: ");
		panel.add(lbAdresse);
		jtfAdresse = new JTextField();
		panel.add(jtfAdresse);
		
		lbTel = new JLabel("Tel: ");
		panel.add(lbTel);
		jtfTel = new JTextField();
		panel.add(jtfTel);
		
		add(panel);

		JPanel panelBoutons = new JPanel();
		btnAjouter = new JButton("Ajouter");
		btnQuitter = new JButton("Quitter");
		panelBoutons.add(btnAjouter);
		panelBoutons.add(btnQuitter);
	
		add(panelBoutons, BorderLayout.SOUTH);
		
		btnAjouter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Entreprise entreprise = Entreprise.addEntreprise(jtfSiret.getText(), jtfNom.getText(), jtfAdresse.getText(), jtfTel.getText());
				table.addEntreprise(entreprise);
				setVisible(false);
			}
		});
		
		btnQuitter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});
		
		setVisible(true);
		
	}
	
}
