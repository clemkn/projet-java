package graph.gui;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import models.Role;
import models.User;

public class RolesJTable extends AbstractTableModel {

	
	private final String[] entetes = {"Nom", "Description"};
	private ArrayList<Role> roleList;
	
	public RolesJTable() {
		// TODO Auto-generated constructor stub
		roleList = Role.getRoles();
		
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return roleList.size();
	}
	
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Role r = roleList.get(rowIndex);
        Object[] values=new Object[]{r.getName(), r.getDescription()};
        return values[columnIndex];
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	    if(aValue != null){
	        Role role = roleList.get(rowIndex);
	 
	        switch(columnIndex){
	            case 0:
	            	role.setName((String)aValue);
	                break;
	            case 1:
	            	role.setDescription((String)aValue);
	                break;
	        }
	    }
	}
	
	public void removeRole(int rowIndex) {
		roleList.remove(rowIndex);
		fireTableRowsDeleted(rowIndex, rowIndex);
	}
	
	public void addRole(Role role){
		roleList.add(role);
		fireTableRowsInserted(roleList.size()-1, roleList.size()-1);
	}

	
	
}
