package graph.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import models.Role;
import models.User;

public class RolesFrame extends JFrame implements ActionListener {

	private JTable table;
	private RolesJTable rolesTable;
	private JButton btnAjouter, btnModifier, btnSupprimer;
	private String roleName;

	public RolesFrame() {
		// TODO Auto-generated constructor stub
		setTitle("Gestion des rôles");
		setSize(600, 300);
		
		table = new JTable(rolesTable=new RolesJTable());
		table.setVisible(true);
		add(new JScrollPane(table), BorderLayout.CENTER);
		
		
		btnAjouter = new JButton("Ajouter");
		btnModifier = new JButton("Modifier");
		btnSupprimer = new JButton("Supprimer");
		
		JPanel panelBoutons=new JPanel();
		panelBoutons.add(btnAjouter);
		panelBoutons.add(btnModifier);
		panelBoutons.add(btnSupprimer);
		add(panelBoutons, BorderLayout.SOUTH);
		
		btnAjouter.addActionListener(this);
		btnModifier.addActionListener(this);
		btnSupprimer.addActionListener(this);
		
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(table.getSelectedRow() > -1 ){
			roleName = table.getValueAt(table.getSelectedRow(), 0).toString();
		}
		
		if(e.getSource() == btnAjouter ){
			
			AddRoleFrame ajouter = new AddRoleFrame(rolesTable);
			ajouter.setVisible(true);
			
		} else if(e.getSource() == btnModifier ){
			
			if(table.getSelectedRow() > -1 ){
				UpdateRoleFrame update = new UpdateRoleFrame(roleName, table.getSelectedRow(), rolesTable);
				update.setVisible(true);
			}  else {
				JOptionPane.showMessageDialog(null,"Sélectionner un élement!");
			}
			
		} else if(e.getSource() == btnSupprimer ){
			if(table.getSelectedRow() > -1 ){
			int[] selection=table.getSelectedRows();
			for(int i=selection.length-1;i>=0;i--)
				rolesTable.removeRole(selection[i]);
			
			Role.deleteRole(roleName);
			}  else {
				JOptionPane.showMessageDialog(null,"Sélectionner un élement!");
			}
		}
	}

	
	
}
