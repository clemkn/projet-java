package graph.gui;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import models.Entreprise;
import models.User;

public class EntreprisesJTable extends AbstractTableModel {
	
	private final String[] entetes = {"SIRET", "Nom"};
	private ArrayList<Entreprise> entrepriseList;
	
	public EntreprisesJTable() {
		entrepriseList = Entreprise.getEntreprises();
	}
	
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return entrepriseList.size();
	}
	
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Entreprise e = entrepriseList.get(rowIndex);
        Object[] values=new Object[]{e.getSiret(),e.getNom()};
        return values[columnIndex];
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	    if(aValue != null){
	        Entreprise entreprise = entrepriseList.get(rowIndex);
	 
	        switch(columnIndex){
	            case 0:
	                entreprise.setSiret((String)aValue);
	                break;
	            case 1:
	            	entreprise.setNom((String)aValue);
	                break;
	            case 2:
	            	entreprise.setAdresse((String)aValue);
	                break;
	            case 3:
	            	entreprise.setTel((String)aValue);
	                break;
	        }
	    }
	}
	
	public void removeEntreprise(int rowIndex) {
		entrepriseList.remove(rowIndex);
		fireTableRowsDeleted(rowIndex, rowIndex);
	}
	
	public void addEntreprise(Entreprise entreprise){
		entrepriseList.add(entreprise);
		fireTableRowsInserted(entrepriseList.size()-1, entrepriseList.size()-1);
	}
}
