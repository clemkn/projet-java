package graph.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import models.User;

public class UsersFrame extends JFrame implements ActionListener {


	private JButton btnAjouter, btnModifier, btnSupprimer, btnDetail;
	private JTable table;
	private DetailsUserFrame details;
	private UsersJTable usersTable;
	private int id_user;
	
	public UsersFrame(){
		
		setTitle("Gestion des utilisateurs");
		setSize(600, 300);
		
		table = new JTable(usersTable=new UsersJTable());
		table.setVisible(true);
		add(new JScrollPane(table), BorderLayout.CENTER);
		
		btnAjouter = new JButton("Ajouter");
		btnDetail = new JButton("Details");
		btnSupprimer = new JButton("Supprimer");
		
		JPanel panelBoutons=new JPanel();
		panelBoutons.add(btnAjouter);
		panelBoutons.add(btnDetail);
		panelBoutons.add(btnSupprimer);
		add(panelBoutons, BorderLayout.SOUTH);
		
		btnAjouter.addActionListener(this);
		btnDetail.addActionListener(this);
		btnSupprimer.addActionListener(this);
		
		setVisible(true);
	}
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if(table.getSelectedRow() > -1 ){
			id_user = Integer.parseInt( table.getValueAt(table.getSelectedRow(), 0).toString() );
		}
		
		if(e.getSource() == btnAjouter){
			
			//AjouterUserFrame ajouter = new AjouterUserFrame();
			RegisterFrame ajouter = new RegisterFrame(usersTable);
			ajouter.setVisible(true);
			
		} else if(e.getSource() == btnDetail){
			
			if(table.getSelectedRow() > -1 ){
				details = new DetailsUserFrame(id_user, table.getSelectedRow(), usersTable) ;
			} else {
				JOptionPane.showMessageDialog(null,"Sélectionner un élement!");
			}
			
		} else if(e.getSource() == btnSupprimer){
			
			if(table.getSelectedRow() > -1 ){
				int[] selection=table.getSelectedRows();
				for(int i=selection.length-1;i>=0;i--)
					usersTable.removeUser(selection[i]);
				
				User.deleteUser(id_user);
			} else {
				JOptionPane.showMessageDialog(null,"Sélectionner un élement!");
			}

			
		}
	}
	
	
}
