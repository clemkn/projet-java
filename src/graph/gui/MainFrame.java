package graph.gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MainFrame extends JFrame {


	public MainFrame(){
		setTitle("Menu");
		setSize(300,350);
		setLocation(500,280); 
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		
		JButton btnGUsers = new JButton("Gestion des utilisateurs");
		JButton btnGClients = new JButton("Gestion des clients");
		JButton btnGRoles = new JButton("Gestion des rôles");
		JButton btnLogOut = new JButton("Deconnexion");
		
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.ipadx = 20;
        gbc.ipady = 20;

        panel.add(btnGUsers, gbc);
        gbc.gridy++;
        panel.add(btnGClients, gbc);
        gbc.gridy++;
        panel.add(btnGRoles, gbc);
        gbc.gridy++;
        panel.add(btnLogOut, gbc);
		
		btnGUsers.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				UsersFrame users = new UsersFrame();
			}
		});

		btnGClients.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ClientsFrame clients = new ClientsFrame();
			}
		});
		
		btnGRoles.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				RolesFrame roles = new RolesFrame();
			}
		});
		
		
		btnLogOut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
				LoginFrame login = new LoginFrame();
			}
		});

		add(panel);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setVisible(true);
		
		
	}
}
