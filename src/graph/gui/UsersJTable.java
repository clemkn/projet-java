package graph.gui;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import models.Entreprise;
import models.User;

public class UsersJTable extends AbstractTableModel {
	
	private final String[] entetes = {"ID", "Prénom", "Nom", "Age"};
	private ArrayList<User> userList;
	
	public UsersJTable() {
		// TODO Auto-generated constructor stub
		userList = User.getUsers();
		
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return userList.size();
	}
	
	public String getColumnName(int columnIndex) {
		return entetes[columnIndex];
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		User u = userList.get(rowIndex);
        Object[] values=new Object[]{u.getId(),u.getFirstName(),u.getLastName(),
                u.getAge()};
        return values[columnIndex];
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	    if(aValue != null){
	        User user = userList.get(rowIndex);
	 
	        switch(columnIndex){
	            case 0:
	                user.setId((int)aValue);
	                break;
	            case 1:
	            	user.setFirstName((String)aValue);
	                break;
	            case 2:
	            	user.setLastName((String)aValue);
	                break;
	            case 3:
	            	user.setAge((int)aValue);
	                break;
	        }
	    }
	}
	
	public void removeUser(int rowIndex) {
		userList.remove(rowIndex);
		fireTableRowsDeleted(rowIndex, rowIndex);
	}
	
	public void addUser(User user){
		userList.add(user);
		fireTableRowsInserted(userList.size()-1, userList.size()-1);
	}

	
	
}
