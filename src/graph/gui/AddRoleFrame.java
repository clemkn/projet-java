package graph.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.Entreprise;
import models.Role;

public class AddRoleFrame extends JFrame {

	private JLabel jlName, jlDesc;
	private JTextField jtfName, jtfDesc;
	private JButton btnAjouter, btnQuitter;
	private JPanel panel;
	
	public AddRoleFrame(RolesJTable table){
		setSize(400,150);
		setTitle("Ajouter");
		panel = new JPanel();
		panel.setLayout( new GridLayout(2,2,10,10) );
		
		jlName = new JLabel("Nom: ");
		panel.add(jlName);
		jtfName = new JTextField();
		panel.add(jtfName);
		
		jlDesc = new JLabel("Description: ");
		panel.add(jlDesc);
		jtfDesc = new JTextField();
		panel.add(jtfDesc);
		
		add(panel);
		
		JPanel panelBoutons = new JPanel();
		btnAjouter = new JButton("Ajouter");
		btnQuitter = new JButton("Quitter");
		panelBoutons.add(btnAjouter);
		panelBoutons.add(btnQuitter);
	
		add(panelBoutons, BorderLayout.SOUTH);
		
		btnAjouter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Role role = Role.addRole(jtfName.getText(), jtfDesc.getText());
				table.addRole(role);
				setVisible(false);
			}
		});
		
		btnQuitter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});
		
	}
	
}
