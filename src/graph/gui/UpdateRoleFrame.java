package graph.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.Entreprise;
import models.Role;

public class UpdateRoleFrame extends JFrame {

	private JLabel jlName, jlDesc;
	private JTextField jtfName, jtfDesc;
	private JButton btnModifier, btnQuitter;
	private JPanel panel;
	
	public UpdateRoleFrame(String roleName, int rowIndex, RolesJTable table){
		
		setSize(400,150);
		setTitle("Modifier: " + roleName);
		
		Role role = Role.getRole(roleName);
		
		panel = new JPanel();
		panel.setLayout( new GridLayout(2,2,10,10) );
		
		jlName = new JLabel("Nom: ");
		panel.add(jlName);
		jtfName = new JTextField();
		jtfName.setText(role.getName());
		panel.add(jtfName);
		
		jlDesc = new JLabel("Description: ");
		panel.add(jlDesc);
		jtfDesc = new JTextField();
		jtfDesc.setText(role.getDescription());
		panel.add(jtfDesc);
		
		add(panel);
		
		JPanel panelBoutons = new JPanel();
		btnModifier = new JButton("Modifier");
		btnQuitter = new JButton("Quitter");
		panelBoutons.add(btnModifier);
		panelBoutons.add(btnQuitter);
	
		add(panelBoutons, BorderLayout.SOUTH);
		
		
		btnModifier.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Role.updateRole(role.getId(), jtfName.getText(), jtfDesc.getText());
				table.setValueAt(jtfName.getText(), rowIndex, 0);
				table.setValueAt(jtfDesc.getText(), rowIndex, 1);
				setVisible(false);
			}
		});
		
		btnQuitter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});
	}
	
}
