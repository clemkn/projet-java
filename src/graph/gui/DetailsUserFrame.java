package graph.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.User;

public class DetailsUserFrame extends JFrame{

	private JPanel panel;
	private JLabel lbLogin, lbLastName, lbFirstName, lbRoleName, lbAge;
	private JTextField jtfLogin, jtfLastName, jtfFirstName, jtfAge;
	private JButton btnModifier, btnQuitter;
	
	public DetailsUserFrame(int idUser, int rowIndex, UsersJTable table){
		setSize(300,300);
		User user = User.getUser(idUser);
		setTitle("Details: "+ user.getFirstName() +" "+user.getLastName());
		
		panel = new JPanel();
		panel.setLayout( new GridLayout(4,2,10,10) );
		
		lbLogin = new JLabel();
		lbLogin.setText("Identifiant");
		jtfLogin = new JTextField();
		jtfLogin.setText(user.getLogin());
		
		lbLastName = new JLabel();
		lbLastName.setText("Nom");
		jtfLastName = new JTextField();
		jtfLastName.setText(user.getLastName());
		
		lbFirstName = new JLabel();
		lbFirstName.setText("Prénom");
		jtfFirstName = new JTextField();
		jtfFirstName.setText(user.getFirstName());
		
		lbAge = new JLabel();
		lbAge.setText("Age");
		jtfAge = new JTextField();
		jtfAge.setText(Integer.toString( user.getAge() ));
		
		/*
		lbRoleName = new JLabel();
		lbRoleName.setText("Role: " + user.getRole());
		 */
		
		panel.add(lbLogin);
		panel.add(jtfLogin);
		panel.add(lbLastName);
		panel.add(jtfLastName);
		panel.add(lbFirstName);
		panel.add(jtfFirstName);
		panel.add(lbAge);
		panel.add(jtfAge);
		//panel.add(lbRoleName);
		
		getContentPane().add(panel);
		
		JPanel panelBoutons = new JPanel();
		btnModifier = new JButton("Modifier");
		btnQuitter = new JButton("Quitter");
		panelBoutons.add(btnModifier);
		panelBoutons.add(btnQuitter);
	
		add(panelBoutons, BorderLayout.SOUTH);
		
		setVisible(true);
		
		btnModifier.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				User.updateUser(idUser, jtfLogin.getText(), jtfLastName.getText(), jtfFirstName.getText(), jtfAge.getText());
				table.setValueAt(jtfFirstName.getText(), rowIndex, 1);
				table.setValueAt(jtfLastName.getText(), rowIndex, 2);
				table.setValueAt(Integer.parseInt(jtfAge.getText()), rowIndex, 3);
				setVisible(false);
			}
		});
		
		btnQuitter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});
	}
	
}
