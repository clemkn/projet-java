package graph.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import models.Login;
import graph.gui.RegisterFrame;

public class LoginFrame extends JFrame {
	
	private JPanel panel;
	private JButton btnLogin, btnRegister;
	private JTextField tfUser;
	private JLabel lbUser, lbPassword;
	private JPasswordField pfPassword;
	
	public LoginFrame(){
		
		super("Connexion");
		panel = new JPanel();
		setSize(280,150);
		setLocation(400,250);
		panel.setLayout (null);
		
		lbUser = new JLabel("Identifiant");
		lbUser.setBounds(10, 10, 80, 25);
		this.add(lbUser);

		tfUser = new JTextField(20);
		tfUser.setBounds(100, 10, 160, 25);
		this.add(tfUser);

		lbPassword = new JLabel("Mot de passe");
		lbPassword.setBounds(10, 40, 80, 25);
		this.add(lbPassword);

		pfPassword = new JPasswordField(20);
		pfPassword.setBounds(100, 40, 160, 25);
		this.add(pfPassword);

		btnLogin = new JButton("Connexion");
		btnLogin.setBounds(10, 80, 120, 25);
		this.add(btnLogin);

		btnRegister = new JButton("Inscription");
		btnRegister.setBounds(140, 80, 120, 25);
		this.add(btnRegister);
	
		getContentPane().add(panel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		actionLogin();
		actionRegister();
	}
	
	public void actionRegister(){
		btnRegister.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				RegisterFrame register = new RegisterFrame(null);
				register.setVisible(true);
			}
		});
	}


	public void actionLogin(){
	btnLogin.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent ae) {
			// TODO Auto-generated method stub
			Login loginModel = new Login(tfUser.getText(), pfPassword.getText());
			if(loginModel.loginCheck()) 
			{
				MainFrame main = new MainFrame();
				main.setVisible(true);
				dispose();
				
			} else {
				JOptionPane.showMessageDialog(null,"Mauvais Identifiant / Mot de passe");
				tfUser.setText("");
				pfPassword.setText("");
				tfUser.requestFocus();
			}
		}
	});

	}
	
}
