package graph.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.Entreprise;
import models.User;

public class DetailsEntrepriseFrame extends JFrame {
	
	private JPanel panel;
	private JLabel lbSiret, lbId, lbNom, lbAdresse, lbTel;
	private JTextField jtfSiret, jtfNom, jtfAdresse, jtfTel;
	private JButton btnModifier, btnQuitter;
	
	public DetailsEntrepriseFrame(String idEntreprise, int rowIndex, EntreprisesJTable table){
		setSize(300,300);
		
		Entreprise entreprise = Entreprise.getEntreprise(idEntreprise);
		setTitle("Details: "+ entreprise.getNom());
		panel = new JPanel();
		panel.setLayout( new GridLayout(4,2,10,10) );
		
		lbSiret = new JLabel("SIRET: ");
		panel.add(lbSiret);
		jtfSiret = new JTextField(entreprise.getSiret());
		panel.add(jtfSiret);
		
		lbNom = new JLabel("Nom: ");
		panel.add(lbNom);
		jtfNom = new JTextField(entreprise.getNom());
		panel.add(jtfNom);
		
		lbAdresse = new JLabel("Adresse: ");
		panel.add(lbAdresse);
		jtfAdresse = new JTextField(entreprise.getAdresse());
		panel.add(jtfAdresse);
		
		lbTel = new JLabel("Tel: ");
		panel.add(lbTel);
		jtfTel = new JTextField(entreprise.getTel());
		panel.add(jtfTel);
		
		add(panel);
		
		JPanel panelBoutons = new JPanel();
		btnModifier = new JButton("Modifier");
		btnQuitter = new JButton("Quitter");
		panelBoutons.add(btnModifier);
		panelBoutons.add(btnQuitter);
	
		add(panelBoutons, BorderLayout.SOUTH);
		
		setVisible(true);
		
		btnModifier.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//System.out.println(entreprise.getId());
				Entreprise.updateEntreprise(entreprise.getId(), jtfSiret.getText(), jtfNom.getText(), jtfAdresse.getText(), jtfTel.getText());
				table.setValueAt(jtfSiret.getText(), rowIndex, 0);
				table.setValueAt(jtfNom.getText(), rowIndex, 1);
				setVisible(false);
			}
		});
		
		btnQuitter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});
	}
	
}
