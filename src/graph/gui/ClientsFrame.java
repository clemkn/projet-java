package graph.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import models.Entreprise;
import models.User;

public class ClientsFrame extends JFrame implements ActionListener {

	private JButton btnAjouter, btnModifier, btnSupprimer, btnDetail;
	private JTable table;
	private DetailsEntrepriseFrame details;
	private EntreprisesJTable entreprisesTable;
	private String id_entreprise;
	
	public ClientsFrame() {
		// TODO Auto-generated constructor stub
		setTitle("Gestion des clients");
		setSize(600, 300);
		
		table = new JTable(entreprisesTable=new EntreprisesJTable());
		table.setVisible(true);
		add(new JScrollPane(table), BorderLayout.CENTER);
		
		btnAjouter = new JButton("Ajouter");
		btnDetail = new JButton("Details");
		btnSupprimer = new JButton("Supprimer");
		
		JPanel panelBoutons=new JPanel();
		panelBoutons.add(btnAjouter);
		panelBoutons.add(btnDetail);
		panelBoutons.add(btnSupprimer);
		add(panelBoutons, BorderLayout.SOUTH);
		
		btnAjouter.addActionListener(this);
		btnDetail.addActionListener(this);
		btnSupprimer.addActionListener(this);
		
		setVisible(true);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(table.getSelectedRow() > -1 ){
			id_entreprise = table.getValueAt(table.getSelectedRow(), 0).toString();
		}
		
		if(e.getSource() == btnAjouter){
			
			AddEntrepriseFrame ajouter = new AddEntrepriseFrame(entreprisesTable);
			
		} else if(e.getSource() == btnDetail){
			System.out.println(id_entreprise);

			if(table.getSelectedRow() > -1 ){
				details = new DetailsEntrepriseFrame(id_entreprise, table.getSelectedRow(), entreprisesTable) ;
			} else {
				JOptionPane.showMessageDialog(null,"Sélectionner un élement!");
			}
			
		} else if(e.getSource() == btnSupprimer){
			if(table.getSelectedRow() > -1 ){
				int[] selection=table.getSelectedRows();
				for(int i=selection.length-1;i>=0;i--)
					entreprisesTable.removeEntreprise(selection[i]);
				
				Entreprise.deleteEntreprise(id_entreprise);
			} else {
				JOptionPane.showMessageDialog(null,"Sélectionner un élement!");
			}
		}
	}

}
