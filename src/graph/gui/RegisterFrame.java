package graph.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import models.HibernateUtil;
import models.Role;
import models.User;

public class RegisterFrame extends JFrame implements ActionListener {
	
	private JLabel jlUsername, jlPassword, jlLogin, jlFirstName, jlLastName, jlAge, jlRole;
	private JTextField jtfUsername, jtfLogin, jtfFirstName, jtfLastName, jtfAge;
	private JPasswordField jpfPassword;
	private JButton btnRegister, btnCanceled;
	private UsersJTable table;
	private JComboBox jcbRole;

	public RegisterFrame(AbstractTableModel table){
		if(table != null){
			this.table = (UsersJTable) table;
		}
		
		setTitle("Ajouter un utilisateur");
		setSize(400, 400);
		
		JPanel panel = new JPanel();
		panel.setLayout( new GridLayout(6,2,10,10) );
		panel.add( jlFirstName=new JLabel("Prénom: ") );
		panel.add( jtfFirstName=new JTextField() );
		
		panel.add( jlLastName=new JLabel("Nom: ") );
		panel.add( jtfLastName=new JTextField() );
		
		panel.add( jlLogin=new JLabel("Login: ") );
		panel.add( jtfLogin=new JTextField() );
		
		panel.add( jlPassword=new JLabel("Mot de passe: ") );
		panel.add( jpfPassword=new JPasswordField() );

		panel.add( jlAge=new JLabel("Age: ") );
		panel.add( jtfAge=new JTextField() );
		
		panel.add(jlRole=new JLabel("Role: "));
		jcbRole = new JComboBox();
		
		for (Iterator iterator = 
                Role.getRoles().iterator(); iterator.hasNext();){
			Role role = (Role) iterator.next(); 
			jcbRole.addItem(role.getName());
		}
		panel.add(jcbRole);
		
		add(panel);
		
		JPanel panelBoutons=new JPanel();
		panelBoutons.add(btnRegister=new JButton("Valider"));
		panelBoutons.add(btnCanceled=new JButton("Annuler"));
		add(panelBoutons, BorderLayout.SOUTH);
		
		btnRegister.addActionListener(this);
		btnCanceled.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == btnRegister){
			
			User user = User.createUser(jtfFirstName.getText(), jtfLastName.getText(), jtfLogin.getText(), jpfPassword.getText(),Integer.parseInt( jtfAge.getText() ), jcbRole.getSelectedItem().toString() );
			
			if(table != null){
				table.addUser(user);;
			}
			
			this.setVisible(false);
			
		} else if(e.getSource() == btnCanceled)
			this.setVisible(false);
	}
	
}
