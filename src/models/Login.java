package models;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Login {

	private String login;
	private String password;

	public Login(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}
	
	public boolean loginCheck(){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		try
	    {
	       tx = session.beginTransaction(); 
	       Query q = session.createQuery("FROM User WHERE login = :login AND password = :pass")
	    		   	.setParameter("login", this.login)
	    		   	.setParameter("pass", this.password);
	       if (q.uniqueResult() != null)
	    	   return true;
	    }
	    catch (HibernateException e)
	    {
	       if (tx!=null) tx.rollback();
	       e.printStackTrace(); 
	    }
		return false;
	}
	
	public String getLogin() {
		return login;
	}
	
	public String getPassword() {
		return password;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setPassword(String pass){
		this.password = pass;
	}
	
	
}
