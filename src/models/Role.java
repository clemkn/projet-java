package models;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Role {

	private int id;
	private String name;
	private String description;
	
	public Role(){
		
	}
	
	public Role(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public static ArrayList<Role> getRoles(){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		ArrayList<Role> roles = new ArrayList<Role>();
		
		try {
			
	       tx = session.beginTransaction(); 
	       Query q = session.createQuery("FROM Role");
	       roles = new ArrayList<Role>(q.list());
	       tx.commit();
	       
	    } catch (HibernateException e) {
	       if (tx!=null) tx.rollback();
	       e.printStackTrace(); 
	    }
		
		return roles;
	}
	
	public static Role getRole(String roleName){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		Role role = new Role();
		try
	    {
	       tx = session.beginTransaction(); 
	       Query q = session.createQuery("FROM Role WHERE name = :name")
	    		   .setParameter("name", roleName);
	       role = (Role) q.uniqueResult();
	       tx.commit();
	       
	    } catch (HibernateException e){
	       if (tx!=null) tx.rollback();
	       e.printStackTrace(); 
	    }
		return role;
	}
	
	public static Role addRole(String name, String description){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		Role role = null;
		try {
			tx = session.beginTransaction();
			role = new Role();
			role.setName(name);
			role.setDescription(description);
	        
			session.save(role); 
			tx.commit();
		} catch (HibernateException e){
			
		       if (tx!=null) tx.rollback();
		       e.printStackTrace();      
		}
		
		return role;
	}

	public static void updateRole(int id, String name, String description) {
		// TODO Auto-generated method stub
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		try{
			
			tx = session.beginTransaction();
	         Role role = (Role)session.get(Role.class, id); 
	         role.setName(name);
	         role.setDescription(description);
	         session.update(role);
	         tx.commit();
	         
		} catch (HibernateException e){
			
		       if (tx!=null) tx.rollback();
		       e.printStackTrace();      
		}
	}

	public static void deleteRole(String roleName) {
		// TODO Auto-generated method stub
			Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
			Transaction tx = null;
			try {
				tx = session.beginTransaction();
				Query q = session.createQuery("FROM Role WHERE name = :name")
			    		   .setParameter("name", roleName);
				Role role = (Role) q.uniqueResult();
				session.delete(role);
				tx.commit();
			} catch (HibernateException e){
				
			       if (tx!=null) tx.rollback();
			       e.printStackTrace();      
			}
	}
	
	
}
