package models;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.text.html.HTMLDocument.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class User {
	
	private int id;
	private String firstName;
	private String lastName;
	private int age;
	private String login;
	private String password;
	private Role role;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public User(){}

	public User(int id, String firstName, String lastName, int age,
			String login, String password, Role role) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.login = login;
		this.password = password;
		this.role = role;
	}
	
	public static ArrayList<User> getUsers(){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		ArrayList<User> users = new ArrayList<User>();
		try
	    {
	       tx = session.beginTransaction(); 
	       Query q = session.createQuery("FROM User");
	       users = new ArrayList<User>(q.list());
	       tx.commit();
	       
	    } catch (HibernateException e)
	    {
	       if (tx!=null) tx.rollback();
	       e.printStackTrace(); 
	    }
		
		return users;
	}
	
	public static User getUser(int id_user){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		User user = new User();
		try
	    {
	       tx = session.beginTransaction(); 
	       Query q = session.createQuery("FROM User WHERE id = :id")
	    		   .setParameter("id", id_user);
	       user = (User) q.uniqueResult();
	       tx.commit();
	    }
		
	    catch (HibernateException e)
	    {
	       if (tx!=null) tx.rollback();
	       e.printStackTrace(); 
	    }
		return user;
	}
	
	public static void updateUser(int id, String login, String lastName, String firstName, String age){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		try
	    {
	       tx = session.beginTransaction(); 
	       User user = (User)session.get(User.class, id); 
	       user.setLogin(login);
	       user.setAge( Integer.parseInt(age) );
	       user.setFirstName(firstName);
	       user.setLastName(lastName);
	       session.update(user);
	       tx.commit();
	       
	    } catch (HibernateException e) {
		       if (tx!=null) tx.rollback();
		       e.printStackTrace(); 
		}
	}
	
	public static void deleteUser(int id_user){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("delete User where id = :id");
		q.setParameter("id", id_user);
		q.executeUpdate();
		tx.commit();
	}
	
	public static User createUser(String firstName, String lastName, String login, String password, int age, String roleName){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		User user = null;
		try {
			tx = session.beginTransaction();
			user = new User();
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setLogin(login);
			user.setPassword(password);
			user.setAge(age);
			user.setRole( (Role) session.createQuery("FROM Role WHERE name = :name")
    		   	.setParameter("name", roleName).uniqueResult() );
	        
			session.save(user); 
			tx.commit();
		} catch (HibernateException e){
			
		       if (tx!=null) tx.rollback();
		       e.printStackTrace();      
		}
		
		return user;
	}
}
