package models;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Entreprise {

	private int id;
	private String siret;
	private String nom;
	private String adresse;
	private String tel;
	
	public Entreprise(){}
	
	public Entreprise(int id, String siret, String nom, String adresse,
			String tel) {
		super();
		this.id = id;
		this.siret = siret;
		this.nom = nom;
		this.adresse = adresse;
		this.tel = tel;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSiret() {
		return siret;
	}
	public void setSiret(String siret) {
		this.siret = siret;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	public static ArrayList<Entreprise> getEntreprises(){
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		ArrayList<Entreprise> entreprises = new ArrayList<Entreprise>();
		
		try {
			
	       tx = session.beginTransaction(); 
	       Query q = session.createQuery("FROM Entreprise");
	       entreprises = new ArrayList<Entreprise>(q.list());
	       tx.commit();
	       
	    } catch (HibernateException e) {
	       if (tx!=null) tx.rollback();
	       e.printStackTrace(); 
	    }
		
		return entreprises;
	}
	
	public static Entreprise getEntreprise(String id_entreprise){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		Entreprise entreprise = new Entreprise();
		try
	    {
	       tx = session.beginTransaction(); 
	       Query q = session.createQuery("FROM Entreprise WHERE siret = :siret")
	    		   .setParameter("siret", id_entreprise);
	       entreprise = (Entreprise) q.uniqueResult();
	       tx.commit();
	    }
		
	    catch (HibernateException e)
	    {
	       if (tx!=null) tx.rollback();
	       e.printStackTrace(); 
	    }
		return entreprise;
	}
	
	public static void updateEntreprise(int id, String siret, String nom, String adresse, String tel){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		try{
			
			tx = session.beginTransaction();
	         Entreprise entreprise = (Entreprise)session.get(Entreprise.class, id); 
	         entreprise.setSiret(siret);
	         entreprise.setAdresse(adresse);
	         entreprise.setNom(nom);
	         entreprise.setTel(tel);
	         session.update(entreprise);
	         tx.commit();
	         
		} catch (HibernateException e){
			
		       if (tx!=null) tx.rollback();
		       e.printStackTrace();      
		}
	}
	
	public static void deleteEntreprise(String siret){
			Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
			Transaction tx = null;
			try {
				tx = session.beginTransaction();
				Query q = session.createQuery("FROM Entreprise WHERE siret = :siret")
			    		   .setParameter("siret", siret);
				Entreprise entreprise = (Entreprise) q.uniqueResult();
				session.delete(entreprise);
				tx.commit();
			} catch (HibernateException e){
				
			       if (tx!=null) tx.rollback();
			       e.printStackTrace();      
			}
	}
	
	public static Entreprise addEntreprise(String siret, String nom, String adresse, String tel){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		Entreprise entreprise = null;
		try {
			tx = session.beginTransaction();
			entreprise = new Entreprise();
			entreprise.setSiret(siret);
			entreprise.setAdresse(adresse);
			entreprise.setNom(nom);
			entreprise.setTel(tel);
	        
			session.save(entreprise); 
			tx.commit();
		} catch (HibernateException e){
			
		       if (tx!=null) tx.rollback();
		       e.printStackTrace();      
		}
		
		return entreprise;
	}
	
	
	
}
