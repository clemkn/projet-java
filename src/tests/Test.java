package tests;

import java.util.ArrayList;
import java.util.List; 
import java.util.Iterator; 

import models.HibernateUtil;

import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;

import beans.*;

public class Test {
	
	public static void main(String[] args) { 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession(); 
		Transaction tx = null;
		List<User> result = new ArrayList();
		
	    try
	    {
	       tx = session.beginTransaction(); 
	       result = (List<User>) session.createQuery("FROM User").list();
	       for (Iterator<User> iterator = result.iterator(); iterator.hasNext();)
	       {
	          User user = (User) iterator.next(); 
	          System.out.print("First Name: " + user.getFirstName()); 
	          System.out.print("Last Name: " + user.getLastName()); 
	       }
	       tx.commit();
	    }
	    catch (HibernateException e)
	    {
	       if (tx!=null) tx.rollback();
	       e.printStackTrace(); 
	    }
	    /*finally {
	       session.close(); 
	    }*/

}//end main

}
